# ScopeFun FPGA firmware sources

This is the [Xilinx Artix-7 FPGA](https://www.xilinx.com/products/silicon-devices/fpga/artix-7.html) firmware source code for ScopeFun.

## Getting started

For compiling the FPGA firmware you must install the [Vivado Design Suite (ML Standard Edition free)](https://www.xilinx.com/products/design-tools/vivado/vivado-ml.html) from Xilinx. Recommended Vivado version is [2022.2](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/2022-2.html).

After completing Vivado installation run the "gen_project.tcl" script from Vivado (Tools -> Run Tcl Script). This will create a new project and link ScopeFun sources from "srcs" folder.

## Licensing

ScopeFun FGPA firmware sources are licensed under GNU General Public License v3 (GPLv3). For details please see the COPYING file(s) and file headers.
